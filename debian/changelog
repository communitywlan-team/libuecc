libuecc (7-6) unstable; urgency=medium

  * debian/watch:
    - Switch to api.github.com to check for releases
  * debian/control:
    - Upgraded to policy 4.7.2, no changes required

 -- Sven Eckelmann <sven@narfation.org>  Sat, 01 Mar 2025 20:57:50 +0100

libuecc (7-5) unstable; urgency=medium

  * debian/control:
    - Upgraded to policy 4.6.2, no changes required

 -- Sven Eckelmann <sven@narfation.org>  Sat, 31 Dec 2022 17:30:16 +0100

libuecc (7-4) unstable; urgency=medium

  * debian/control:
    - Upgraded to policy 4.6.1, no changes required
    - Switch to correct DEP-14 unstable branch name
    - Remove versioned constraints unnecessary since buster
  * debian/upstream/metadata:
    - Fix path to upstream repository
  * debian/copyright:
    - Update copyright years to 2022

 -- Sven Eckelmann <sven@narfation.org>  Mon, 14 Nov 2022 18:34:46 +0100

libuecc (7-3) unstable; urgency=medium

  * debian/control:
    - Switch maintainer to Debian CommunityWLAN Team

 -- Sven Eckelmann <sven@narfation.org>  Sun, 12 Jul 2020 12:04:24 +0200

libuecc (7-2) unstable; urgency=medium

  * debian/control:
    - Order control files with `wrap-and-sort -abst`
    - Switch to debhelper compat 13
    - Switch from deprecated extra priority to optional
    - Allow one to build without root
    - Enable multi-arch same
    - Add Vcs-* information
    - Fix Homepage URL
    - Upgraded to policy 4.5.0, no changes required
    - Add myself as uploader as discussed with Maintainer
    - Drop Matthias Schiffer as Maintainer as discussed with him
  * Add debian/upstream/metadata
  * Fix debian/watch upstream URL
  * Provide Build-Depends-Package for dpkg-shlibdeps
  * debian/rules:
    - Define PHONY targets
    - Use standard dh_clean
    - Restructure dh_auto_configure
    - Use debhelper's standard cmake CMAKE_BUILD_TYPE
    - Enable all hardening options
  * Fix debian/copyright format

 -- Sven Eckelmann <sven@narfation.org>  Sun, 31 May 2020 15:13:51 +0200

libuecc (7-1) unstable; urgency=low

  * New upstream version.
  * Added myself as additional uploader.
  * Set hardening flags

 -- Haiko Helmholz <jo-travolti@t-online.de>  Tue, 05 Apr 2016 19:55:45 +0200

libuecc (6-1) unstable; urgency=low

  * Upload of 6-0uf1 with a few changes to debian/{control,changelog,copyright,rules},
    only.
  * Introduced watch file, albeit still encumbered by certificate problems.
  * Added myself to uploaders.

 -- Steffen Moeller <moeller@debian.org>  Sun, 25 Oct 2015 18:26:31 +0100

libuecc (6-0uf1) UNRELEASED; urgency=low

  * New upstream release
  * Use new release number scheme to track package origin (uf suffix for
    universe-factory.net releases)

 -- Matthias Schiffer <mschiffer@universe-factory.net>  Sun, 25 Oct 2015 16:15:40 +0100

libuecc (5-3) UNRELEASED; urgency=low

  * Build with optimization

 -- Matthias Schiffer <mschiffer@universe-factory.net>  Sun, 31 May 2015 22:11:38 +0200

libuecc (5-2) UNRELEASED; urgency=low

  * Add symbol definitions, include static library into -dev package

 -- Matthias Schiffer <mschiffer@universe-factory.net>  Fri, 30 Jan 2015 08:37:36 +0100

libuecc (5-1) UNRELEASED; urgency=low

  * New release

 -- Matthias Schiffer <mschiffer@universe-factory.net>  Thu, 29 Jan 2015 07:17:31 +0100

libuecc (4-1) UNRELEASED; urgency=low

  * New release

 -- Matthias Schiffer <mschiffer@universe-factory.net>  Sun, 29 Dec 2013 13:08:54 +0100

libuecc (3-1) UNRELEASED; urgency=low

  * New release

 -- Matthias Schiffer <mschiffer@universe-factory.net>  Thu, 10 Jan 2013 18:07:21 +0100

libuecc (2-1) UNRELEASED; urgency=low

  * libuecc v2 release

 -- Matthias Schiffer <mschiffer@universe-factory.net>  Sun, 23 Dec 2012 23:20:18 +0100

libuecc (0.1-2) UNRELEASED; urgency=low

  * libuecc v0.1 release

 -- Matthias Schiffer <mschiffer@universe-factory.net>  Sat, 06 Oct 2012 18:33:09 +0200
